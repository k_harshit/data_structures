//
// Created by harshit on 9/15/17.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "binary_search_tree.h"


/* Allocate a new node with the given data and NULL left and right pointers */
BinaryNode *newBinaryNode(int data){
    // allocate memory for new node
    BinaryNode *new_node = malloc(sizeof(BinaryNode));
    // assign data to new node
    new_node->data = data;
    // initialize left and right children as NULL
    new_node->left = NULL;
    new_node->right = NULL;

    return new_node;
}


BinaryNode *insertBinaryNode(BinaryNode *node, int data){
    // if tree is empty, return a new, single node
    if (node == NULL)
        return newBinaryNode(data);
    else{
        // otherwise recurse down the tree
        if (data <= node->data)
            node->left = insertBinaryNode(node->left, data);
        else
            node->right = insertBinaryNode(node->right, data);
        // return the unchanged node pointer
        return node;
    }
}


int searchBinaryNode(BinaryNode *node, int target){
    // base case if target is not found
    if (node == NULL)
        return false;
    else{
        // if found here at node itself
        if (target == node->data)
            return true;
        else{
            // recurse
            if (target < node->data)
                return searchBinaryNode(node->left, target);
            else
                return searchBinaryNode(node->right, target);
        }
    }
}


void traverse_inorder(BinaryNode *node){
    if (node == NULL)
        return;
    traverse_inorder(node->left);
    printf("%d ", node->data);
    traverse_inorder(node->right);
}
