//
// Created by harshit on 11/9/17.
//

#include "linked_list_circular.h"
#include <stdio.h>
#include <stdlib.h>


void print_circular_list(cllnode *head){
    cllnode *temp = head;
    // If linked list is not empty
    if (head != NULL){
        // Keep printing nodes till we reach the first node again
        do {
            printf("%d ", temp->data);
            temp = temp->next;
        }
        while (temp != head);
    }
}


cllnode *create(int new_data){
    // Create a node dynamically
    cllnode *last = malloc(sizeof(cllnode));
    last->data = new_data;
    // link single node to itself
    last->next = last;
    return last;
}


cllnode *insert_circular_list(cllnode *last, int new_data, int item){
    if (last == NULL)
        return NULL;
    cllnode *temp, *p;
    p = last->next;
    do {
        if (p->data == item){
            temp = malloc(sizeof(cllnode));
            temp->data = new_data;
            temp->next = p->next;
            p->next = temp;
            // check for last node
            if (p == last)
                last = temp;
            return last;
        }
        p = p->next;
    } while (p != last->next);
    printf("%d is not present in the list", item);
    return last;
}


void delete_circular_list(cllnode *head, int key){
    if (head == NULL)
        return;
//    if(head->next == head) {
//        printf("\nEmpty Linked List. Deletion not possible.\n");
//        return;
//    }
    cllnode *ptr1;
    while((head->next != head) && (head->data != key))
    {
        ptr1 = head;
        head = head->next;
    }
    if(head->data == key)
    {
        ptr1->next = head->next;
        free(head);
    }
    else
        printf("\nValue %d not found. Deletion not possible.\n", key);
}

