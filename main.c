#include <stdio.h>
#include <stdlib.h>
#include "linked_list.h"
#include "stack.h"
#include "queue.h"
#include "binary_search_tree.h"
#include "linked_list_circular.h"


int main() {


    /* LINKED LIST */

    printf("\nLINKED LIST\n");

    /* initialize nodes */
    sllnode *head = NULL;
    sllnode *second = NULL;
    sllnode *third = NULL;

    /* allocate memory */
    head = malloc(sizeof(sllnode));
    second = malloc(sizeof(sllnode));
    third = malloc(sizeof(sllnode));

    /* assign data values */
    head -> data = 1;
    second -> data = 2;
    third -> data = 3;

    /* connect nodes */
    head -> next = second;  // link first node with the second one
    second->next = third;
    third->next = NULL;

    print_list(head);

    push(&head, 4);
    printf("\n");
    print_list(head);

    append(&head, 5);
    printf("\n");
    print_list(head);

    insert(head->next->next, 6);
    printf("\n");
    print_list(head);

    deleteList(&head, 2);
    printf("\n");
    print_list(head);

    printf("\nNo of nodes in the final list is %d.\n", len(head));

    search(head, 3) ? printf("yes") : printf("no");


    /* Circular LINKED LIST */

    printf("\nLINKED LIST (CIRCULAR)\n");
    cllnode *last = NULL;
    last = create(6);
    print_circular_list(last);
    insert_circular_list(last, 10, 6);
    printf("\n");
    print_circular_list(last);
    insert_circular_list(last, 7, 6);
    printf("\n");
    print_circular_list(last);
    delete_circular_list(last, 7);
    printf("\n");
    print_circular_list(last);


    /* STACK */

    printf("\nSTACK\n");

    Stack *stack1 = createStack(10);


    int n = 10;
    pushS(stack1, n);
    printf("%d added to stack\n", n);
    n = 20;
    pushS(stack1, n);
    printf("%d added to stack\n", n);
    n = 30;
    pushS(stack1, n);
    printf("%d added to stack\n", n);

    printf("%d popped from stack\n", popS(stack1));

    /* STACKL */

    printf("\nSTACK using Linked list\n");
    StackL *stack2 = newNode(2);
    pushSL(&stack2, 3);
    pushSL(&stack2, 5);
    printf("%d popped from stack\n", popSL(&stack2));


    /* QUEUE */

    printf("\nQUEUE\n");

    Queue *queue1 = createQueue(1000);

    enqueue(queue1, 10);
    enqueue(queue1, 20);

    printf("%d dequeued from queue\n", dequeue(queue1));


    /* Binary Search Tree */

    printf("\nBinary Search Tree\n");

    BinaryNode *binaryNode1 = newBinaryNode(1);
    binaryNode1->left = newBinaryNode(2);
    binaryNode1->right = newBinaryNode(3);
    binaryNode1->left->left = newBinaryNode(4);

    // 1 for true and 0 for false
    printf("%d\n", searchBinaryNode(binaryNode1, 6));
    insertBinaryNode(binaryNode1, 6);
    printf("%d\n", searchBinaryNode(binaryNode1, 6));

    traverse_inorder(binaryNode1);


    /* NOTATIONS */

    printf("\nNOTATIONS\n");

    char E[50];
    printf("Enter your postfix expression (don't use space characters): ");
    scanf("%s", E);
    int result = calculatePostfix(E);
    printf("The result of the postfix expression %s = %d.\n", E, result);



    return 0;
}
