//
// Created by root on 9/5/17.
//

#ifndef DATA_STRUCTURES_MAIN_H
#define DATA_STRUCTURES_MAIN_H

#endif //DATA_STRUCTURES_MAIN_H


typedef struct stack{
    int top;
    unsigned capacity;
    int *array;
}Stack;


Stack *createStack(unsigned capacity);
int isEmpty(Stack *stack);
int isFull(Stack *s);
void pushS(Stack *s, int item);
int popS(Stack *s);
int calculatePostfix(char *exp);


typedef struct stackNode{
    int data;
    struct stackNode *next;
}StackL;

StackL *newNode(int new_data);
int isEmptyL(StackL *rootNode);
void pushSL(StackL **s, int item);
int popSL(StackL **s);
