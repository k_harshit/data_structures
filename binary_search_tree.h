//
// Created by harshit on 9/15/17.
//

#ifndef DATA_STRUCTURES_BINARY_SEARCH_TREE_H
#define DATA_STRUCTURES_BINARY_SEARCH_TREE_H

#endif //DATA_STRUCTURES_BINARY_SEARCH_TREE_H

typedef struct binaryNode{
    int data;  // node itself
    struct binaryNode *left;  // pointer to left child
    struct binaryNode *right;  // pointer to right child
}BinaryNode;

BinaryNode *newBinaryNode(int data);
BinaryNode *insertBinaryNode(BinaryNode *node, int data);
int searchBinaryNode(BinaryNode *node, int target);
void traverse_inorder(BinaryNode *node);
