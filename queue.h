//
// Created by root on 9/5/17.
//

#ifndef DATA_STRUCTURES_QUEUE_H
#define DATA_STRUCTURES_QUEUE_H

#endif //DATA_STRUCTURES_QUEUE_H

typedef struct queue{
    int head, tail, size;
    unsigned capacity;
    int *arr;
}Queue;


Queue *createQueue(unsigned capacity);
int isQFull(Queue *queue);
int isQEmpty(Queue *queue);
void enqueue(Queue *queue, int item);
int dequeue(Queue *queue);
