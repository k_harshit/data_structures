#include <stdio.h>
#include <stdlib.h>
#include "linked_list.h"
#include <stdbool.h>


void print_list(sllnode *n){
    while(n != NULL){
        printf("%d ", n->data);
        n = n->next;
    }
}


void push(sllnode **head, int new_data){
    sllnode *new_node = malloc(sizeof(sllnode));
    new_node->data = new_data;
    new_node->next = *head;
    *head = new_node;
}


void append(sllnode **head, int new_data){
    sllnode *new_node = malloc(sizeof(sllnode));
    sllnode *last = *head;
    new_node->data = new_data;
    new_node->next = NULL;
    if(*head == NULL){
        *head = new_node;
        return;
    }
    while(last->next != NULL)
        last = last->next;
    last->next = new_node;
}


void insert(sllnode *prev_node, int new_data){
    // check if given previous node is NULL
    if(prev_node == NULL){
        printf("The given previous node can\'t be NULL!");
        return;
    }
    // allocate new node
    sllnode *new_node = malloc(sizeof(sllnode));
    new_node->data = new_data;
    // make next of next node as next of previous node
    new_node->next = prev_node->next;
    // move the next of previous node as new node
    prev_node->next = new_node;
}


void deleteList(sllnode **head, int key){
    // store head node
    sllnode *temp = *head, *prev;
    // if head node itself holds the key to be deleted
    if(temp != NULL && temp->data == key){
        *head = temp->next;  // change head
        free(temp);  // free old head
        return;
    }
    // search for key to be deleted
    while(temp != NULL && temp->data != key){
        prev = temp;
        temp = temp->next;
    }
    // if key not present in the list
    if(temp == NULL)
        return;
    // unlink node from the list
    prev->next = temp->next;
    free(temp);
}


int len(sllnode *head){
    int count = 0;
    while(head != NULL){
        count++;
        head = head->next;
    }
    return count;
}


int len_rec(sllnode *head){
    if(head == NULL)
        return 0;
    else
        return 1 + len_rec(head->next);
}


bool search(sllnode *head, int x){
    while (head != NULL){
        if (head->data == x)
            return true;
        head = head->next;
    }
    return false;
}


bool search_rec(sllnode *head, int x){
    if (head == NULL)
        return false;
    if (head->data == x)
        return true;
    return search(head->next, x);
}
