//
// Created by harshit on 9/5/17.
//


#include <stdio.h>
#include <stdlib.h>
#include "limits.h"
#include "queue.h"


Queue *createQueue(unsigned capacity){
    Queue *new_queue = malloc(sizeof(Queue));
    new_queue->capacity = capacity;
    new_queue->head = 0;
    new_queue->size = 0;
    new_queue->tail = capacity -1;
    new_queue->arr = malloc(sizeof(new_queue->capacity * sizeof(int)));
    return new_queue;
}


int isQFull(Queue *queue){
    return (queue->size == queue->capacity);
}


int isQEmpty(Queue *queue){
    return (queue->size == 0);
}


void enqueue(Queue *queue, int item){
    if (isQFull(queue))
        return;
    queue->tail = (queue->tail + 1)%queue->capacity;
    queue->arr[queue->tail] = item;  // add item at the tail of queue
    queue->size += 1;
    printf("%d enqueued to queue\n", item);
}


int dequeue(Queue *queue){
    if (isQEmpty(queue))
        return INT_MIN;
    int item = queue->arr[queue->head];  // head element of queue
    queue->head = (queue->head + 1)%queue->capacity;
    queue->size -= 1;
    return item;
}
