//
// Created by root on 11/9/17.
//

#ifndef DATA_STRUCTURES_LINKED_LIST_CIRCULAR_H
#define DATA_STRUCTURES_LINKED_LIST_CIRCULAR_H

#endif //DATA_STRUCTURES_LINKED_LIST_CIRCULAR_H

typedef struct cllist{
    int data;
    struct cllist *next;
}cllnode;


void print_circular_list(cllnode *head);
cllnode *create(int new_data);
cllnode *insert_circular_list(cllnode *last, int new_data, int item);
void delete_circular_list(cllnode *head, int key);
