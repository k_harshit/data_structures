//
// Created by harshit on 9/4/17.
//

#include <stdbool.h>

#ifndef DATA_STRUCTURES_HELPERS_H
#define DATA_STRUCTURES_HELPERS_H

#endif //DATA_STRUCTURES_HELPERS_H

/*
typedef struct sllist{  // temporary name: sllist
    VALUE val;
    struct sllist* next;  // self-referencing pointer
}sllnode;  // permanent name: sllnode

// Now, we can refer this node as sllnode
*/

typedef struct sllist{
    int data;
    struct sllist *next;
}sllnode;

void print_list(sllnode *n);
void push(sllnode **head, int new_data);
void append(sllnode **head, int new_data);
void insert(sllnode *prev_node, int new_data);
void deleteList(sllnode **head, int key);
int len(sllnode *head);
int len_rec(sllnode *head);
bool search(sllnode *head, int x);
bool search_rec(sllnode *head, int x);
