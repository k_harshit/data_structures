//
// Created by harshit on 9/5/17.
//

/*
Each stack operation is O(1): Push, Pop, Top, Empty.

A pointer called TOP is used to keep track of the top element in the stack.
When initializing the stack, we set its value to -1 so that we can check if the stack is empty by comparing TOP == -1.
On pushing an element, we increase the value of TOP and place the new element in the position pointed to by TOP.
On popping an element, we return the element pointed to by TOP and reduce its value.
Before pushing, we check if stack is already full
Before popping, we check if stack is already empty
*/

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "stack.h"


/* initializes size of stack as 0 */
Stack *createStack(unsigned capacity){
    Stack *new_stack = malloc(sizeof(Stack));
    new_stack->capacity = capacity;
    new_stack->top = -1;
    new_stack->array = malloc(new_stack->capacity * sizeof(int));
    return new_stack;
}


/* Stack is empty when top is equal to -1 */
int isEmpty(Stack *s){
    return s->top == -1;
}


/* Stack is full when top is equal to the last index */
int isFull(Stack *s){
    return s->top == s->capacity - 1;  // s.index = |0|1|2|3|4| => s.top = 4 and s.capacity = 5
}


void pushS(Stack *s, int item){
    if (isFull(s))
        return;
    s->top++;  // S.top = S.top + 1
    s->array[s->top] = item;  // S[S.top] = item; increase s.top then put item in it
}


int popS(Stack *s){
    if (isEmpty(s))
        return INT_MIN;
    s->top--;  // S.top = S.top - 1
    return s->array[s->top + 1];  // S[S.top + 1] : return the popped element
    /* above two lines can be written as [++s->top] but not [s->top++] */
}




StackL *newNode(int new_data){
    StackL *stackNewNode = malloc(sizeof(struct stackNode));
    stackNewNode->data = new_data;
    stackNewNode->next = NULL;
    return stackNewNode;
}


int isEmptyL(StackL *rootNode){
    return !rootNode;
}

void pushSL(StackL **s, int item){
    /* by *s:  we're giving 'push' the pointer to first element, so it'll copy the pointer to first element and update
     it but the original pointer will remain same (pass by value), that's why we pass (**s) pointer to pointer of first
     element (pass by reference). */
    // linked list is dynamic so never full
    StackL *stackNewNode = newNode(item);
    stackNewNode->next = *s;  // | |  | |<-2-<-1- : dereference the 1st pointer and point new node to it
    *s = stackNewNode;  /* update pointer to new added element (dereference the first pointer as we have pointer to
      pointer to head of the list and modify the second one): | | |<-2-<-1- => -2->| | | |  */
    printf("%d pushed to stack\n", item);
}

int popSL(StackL **s){
    if (isEmptyL(*s))  // | |<-2-<-1- : if 1st pointer is NULL
        return INT_MIN;
    int popped = (*s)->data;
    *s = (*s)->next;  // -2->-1->|a|-->|b|  => -1->|b|
    return popped;  // (*s)->data of initial s pointer
}



int calculatePostfix(char *exp){
    /* When a number is seen it is pushed onto the stack; when an operator is seen the operator is applied to the
     * two numbers popped from the stack and the result is pushed back to the stack. */
    Stack *s = createStack(20);
//    s->top = -1;
    int i, num1, num2, value;
    for (i = 0; exp[i] != '\0'; ++i) {
        // checks if exp[i] has a digit
        if (exp[i] >= '0' && exp[i] <= '9')
            // converts digit into integer
            pushS(s, (int)(exp[i] - '0'));
        else{
            num1 = popS(s);
            num2 = popS(s);
            switch (exp[i]){
                case '+': value=num2+num1;
                    break;
                case '-': value=num2-num1;
                    break;
                case '*': value=num2*num1;
                    break;
                case '/': value=num2/num1;
                    break;
                default: printf("Illegal operator!\n");
                    exit(1);
            }
            pushS(s, value);
        }
    }
    return popS(s);
}

